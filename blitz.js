if (process.version.slice(1).split(".")[0] < 8) throw new Error("Update your node shithead");

const ignoreEvents = ["GUILD_BAN_ADD", "GUILD_BAN_REMOVE", "CHANNEL_CREATE", "CHANNEL_DELETE", "RELATIONSHIP_ADD", "RELATIONSHIP_REMOVE", "TYPING_START"];
const Discord = require("discord.js");
const DiscordRPC = require("discord-rpc");

const { promisify } = require("util");
const readline = require("readline");
const bList = require("./util/banned.json");
const readdir = promisify(require("fs").readdir);
const fs = require('fs');
const Enmap = require("enmap");
const EnmapLevel = require("enmap-level");
const Provider = require("enmap-sqlite");
const chalk = require("chalk");
const moment = require("moment");
require("moment-duration-format");

const { Client } = require('pg');

const pg = new Client({
  host: '127.0.0.1',
  port: 5432,
  database: 'blitzdb',
  user: 'lawbreaker',
  password: 'yxcvbnm123',
});

const client = new Discord.Client({
  disabledEvents: ignoreEvents
});

client.rpc = new DiscordRPC.Client({ transport: 'ipc' });

client.pg = pg;
client.banned = bList;
client.config = require("./config.js");
client.logger = require("./util/Logger");

require("./modules/functions.js")(client);

client.commands = new Enmap();
client.aliases = new Enmap();
client.settings = new Enmap({provider: new EnmapLevel({name: "settings"})});
client.economy = new Enmap({provider: new Provider({name: "economy"})});

const rl = readline.createInterface({ input: process.stdin, output: process.stdout });

const generateLaunchCode = () => {
  let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
  let code = "";
  for (let c = 0; c < 6; c++) { 
    code = code + possible.charAt(Math.floor(Math.random() * Math.floor(possible.length + 1)));
  }
  return code;
}  

const update = async () => {
  client.setInterval(() => {
    client.banned = bList;
    // let duration = moment.duration(client.uptime).format("H [h], m [m], s [s]");
    client.rpc.setActivity({
      details: `Serving ${client.guilds.size} guilds!`,
      state: ``,
      largeImageKey: 'main_big',
      largeImageText: 'Made by Lawbreaker',
      smallImageKey: 'main_small',
      smallImageText: 'Based on NodeJS',
      instance: false,
    });
  }, 1000);
}

const init = async () => {

  const cmdFiles = await readdir("./commands/");
  cmdFiles.forEach(f => {
    if (!f.endsWith(".js")) return;
    const response = client.loadCommand(f);
    if (response) console.log(response);
  });

  const evtFiles = await readdir("./events/");
  evtFiles.forEach(file => {
    const eventName = file.split(".")[0];
    const event = require(`./events/${file}`);
    client.on(eventName, event.bind(null, client));
    delete require.cache[require.resolve(`./events/${file}`)];
  });

  client.levelCache = {};
  for (let i = 0; i < client.config.permLevels.length; i++) {
    const thisLevel = client.config.permLevels[i];
    client.levelCache[thisLevel.name] = thisLevel.level;
  }

  client.login(client.config.token);
  DiscordRPC.register('463273929369321472');
  client.rpc.login('463273929369321472').catch(console.error);
  
};

const launch = async () => {
  let lcode = generateLaunchCode();
  rl.question(`${chalk.cyan(`Please enter launch code ${chalk.red(`${lcode}`)} to start the bot: `)}`, answer => {
    if ( answer !== lcode ) {
      console.log(`${chalk.red("You entered the wrong code. Terminating process.")}`);
      process.exit();
    }
    console.log(`${chalk.green(`Launch permission is granted. Please wait...`)}`);
    rl.close();
    update();
    init();
  });
}

launch();

client.rpc.on('ready', () => {
  console.log(`${chalk.cyan("RPC Addon loaded.")}`);
  update();
});

