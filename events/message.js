const bList = require('../util/banned.json');

module.exports = (client, message) => {
  if (message.author.bot && (message.author.id !== '463274410732945420')) return;
  if (bList.users.includes(message.author.id)) return;

  if (message.guild) {

    const entrykey = `${message.author.id}-eco`;

    if(client.economy.has(entrykey)) {

    let currentexp = client.economy.getProp(entrykey, "exp");

    client.economy.setProp(entrykey, "exp", ++currentexp);

    const curLevel = Math.floor(0.1 * Math.sqrt(currentexp));

    if (client.economy.getProp(entrykey, "level") < curLevel) {
      message.reply(`***Insert fancy level up message here***`);
      client.economy.setProp (entrykey, "level", curLevel);
    }

    if (client.economy.getProp(entrykey, "level") >= 5) { client.economy.setProp(entrykey, "slots", 3); }
    if (client.economy.getProp(entrykey, "level") >= 10) { client.economy.setProp(entrykey, "slots", 4); }
    if (client.economy.getProp(entrykey, "level") >= 20) { client.economy.setProp(entrykey, "slots", 5); }
    if (client.economy.getProp(entrykey, "level") >= 50) { client.economy.setProp(entrykey, "slots", 6); }
    
    }
  }

  const settings = message.settings = client.getGuildSettings(message.guild);

  if (message.content.indexOf(settings.prefix) !== 0) return;

  const args = message.content.slice(settings.prefix.length).trim().split(/ +/g);
  const command = args.shift().toLowerCase();

  const level = client.permlevel(message);


  const cmd = client.commands.get(command) || client.commands.get(client.aliases.get(command));
  if (!cmd) return;


  if (cmd && !message.guild && cmd.conf.guildOnly)
    return message.channel.send("This command is unavailable via private message. Please run this command in a guild.");

  if (level < client.levelCache[cmd.conf.permLevel]) {
    if (settings.systemNotice === "true") {
      return message.channel.send(`You do not have permission to use this command.
  Your permission level is ${level} (${client.config.permLevels.find(l => l.level === level).name})
  This command requires level ${client.levelCache[cmd.conf.permLevel]} (${cmd.conf.permLevel})`);
    } else {
      return;
    }
  }


  message.author.permLevel = level;
  
  message.flags = [];
  while (args[0] && args[0][0] === "-") {
    message.flags.push(args.shift().slice(1));
  }
  // If the command exists, **AND** the user has permission, run it.
  client.logger.cmd(`[CMD] ${client.config.permLevels.find(l => l.level === level).name} ${message.author.username} (${message.author.id}) ran command ${cmd.help.name}`);
  cmd.run(client, message, args, level);
};
