const chalk = require('chalk');
const moment = require('moment');
const { version } = require("discord.js");
module.exports = async client => {
  const duration = moment.duration(client.uptime).format(" D [days], H [hrs], m [mins], s [secs]");
  // Log that the bot is online.
  console.log(chalk.cyan([
    `\n/==================== Started at ${chalk.yellow(moment(client.readyAt).format('H:mm:ss'))} ====================/`,
    `| Logged in bot: ${chalk.yellow(client.user.username)}.`,
    `| ${chalk.white(`Bot is serving ${client.guilds.size} servers. Logging sysinfo:`)}`,
    `${chalk.black('_________________')}${chalk.cyan('• Mem Usage')}  :: ${(process.memoryUsage().heapUsed / 1024 / 1024).toFixed(2)} MB`,
    `${chalk.black('_________________')}${chalk.cyan('• Discord.js')} :: v${version}`,
    `${chalk.black('_________________')}${chalk.cyan('• Node')}       :: ${process.version}`,
    `| ${chalk.white('Logging was successful. Waiting for orders...')}`,
    `| Use ${chalk.yellow('Control + C')} to exit process. Use ${chalk.yellow('Cmd + C')} for Mac.`,
    `/=============================================================/`
  ].join('\n')));

  client.user.setActivity(`${client.config.defaultSettings.prefix}help`, {type: "WATCHING"});

  const setTimestamps = () => {
    let noonUnix = new Date();
    if(noonUnix.getHours()>=12) noonUnix.setDate(noonUnix.getDate()+1);
    noonUnix.setHours(12, 0, 0, 0);
    client.nextday = Date.parse(noonUnix);
    client.logger.log(`[LOG] Loaded all settings successfully.`);
  }
  
  setTimestamps();
  
  client.setInterval(() => {
    if (Date.now() > client.nextday) {
      client.resetDailies();
      setTimestamps();
    }
  }, 10 * 60 * 1000);
};