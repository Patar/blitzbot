const chalk = require("chalk");
const moment = require("moment");

exports.log = (content, type = "log") => {
  const timestamp = `[${moment().format("YYYY-MM-DD HH:mm:ss")}]:`;
  switch (type) {
    case "log": {
      return console.log(`[${chalk.cyan(timestamp)}]${chalk.bgBlue.bold(' Logging ')} ${content}`);
    }
    case "warn": {
      return console.log(`[${chalk.cyan(timestamp)}]${chalk.bgYellow.bold(' Warning ')} ${content}`);
    }
    case "error": {
      return console.log(`[${chalk.cyan(timestamp)}]${chalk.bgRed.bold(' Error ')} ${content}`);
    }
    case "debug": {
      return console.log(`[${chalk.cyan(timestamp)}]${chalk.bgGreen.bold(' Debug ')} ${content}`);
    }
    case "cmd": {
      return console.log(`[${chalk.cyan(timestamp)}]${chalk.bgBlue.bold(' Command ')} ${content}`);
    }
    case "ready": {
      return console.log(`[${chalk.cyan(timestamp)}]${chalk.bgGreen.bold(' Ready ')} ${content}`);
    }
    default: throw new TypeError("Logger type must be either warn, debug, log, ready, cmd or error.");
  }
}; 

exports.error = (...args) => this.log(...args, "error");

exports.warn = (...args) => this.log(...args, "warn");

exports.debug = (...args) => this.log(...args, "debug");

exports.cmd = (...args) => this.log(...args, "cmd");
