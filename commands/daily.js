const { Canvas } = require("canvas-constructor");
const { Attachment } = require("discord.js");
const fsn = require('fs-nextra');

async function createDaily(reward){
  const image = await fsn.readFile('./images/dailygold.png');

  return new Canvas(150, 150)
  .addImage(image, 0, 0, 150, 150)
  .setColor('#d4af37')
  .setTextFont('12pt Impact')
  .setTextAlign('center')
  .addText(`You earned ${reward.toLocaleString()} credits`, 75, 75)
  .toBuffer();
}

// eslint-disable-next-line no-unused-vars
exports.run = async (client, message, args, level) => {

    const entrykey = `${message.author.id}-eco`;

    if(client.economy.has(entrykey)) {
    
    if(client.economy.getProp(entrykey, 'cmdcd') == true) return message.reply(`You can only get rewarded once a day.`);

    let creds = client.economy.getProp(entrykey, "credits");
    let boost = client.economy.getProp(entrykey, "elite");

    if(boost == true) {
      let min = 100; let max = 300;
      const newReward = Math.floor(Math.random() * (max - min + 1)) + min;
      client.economy.setProp(entrykey, "credits", creds + newReward);
      client.economy.setProp(entrykey, "cmdcd", true);
      await message.channel.send(new Attachment(await createDaily(newReward, `dailycard.png`)));
    } else {
      let min = 250; let max = 500;
      const newReward = Math.floor(Math.random() * (max - min + 1)) + min;
      client.economy.setProp(entrykey, "credits", creds + newReward);
      client.economy.setProp(entrykey, "cmdcd", true);
      await message.channel.send(new Attachment(await createDaily(newReward, `dailycard.png`)));
    }
  } else {
    message.channel.send('Please create a profile using \`b!register\`');
  }
  };
  
  exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: ["d", "dailies", "reward"],
    permLevel: "User"
  };
  
  exports.help = {
    name: "daily",
    category: "economy",
    description: "Claim a daily reward",
    usage: "daily [user]"
  };