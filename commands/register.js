        //client.economy.set(entrykey, {
        //  user: message.author.id, exp: 0, level: 0, elite: false, credits: 0, prem: 0, slots: 2, cmdcd: false
        //});

// eslint-disable-next-line no-unused-vars
exports.run = async (client, message, args, level) => {
    const entrykey = `${message.author.id}-eco`;
    if(!client.economy.has(entrykey)) {
    
      const choice = await client.awaitReply(message, `Reply with either you want choose: \`knight\`, \`archer\` or \`mage\`. Remember that you cannot change the class.`);

      if (["knight", "archer", "mage"].includes(choice.toLowerCase())) {
        client.economy.set(entrykey, {
          user: message.author.id, exp: 0, level: 0, elite: false, credits: 0, prem: 0, slots: 2, cmdcd: false, class: "choice"
        });
      }
    message.channel.send(`${message.author.username}, you successfully created a profile. View it with \`b!profile\`!`);
    }
    
  };
  
  exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: ["register", "create", "signup"],
    permLevel: "User"
  };
  
  exports.help = {
    name: "register",
    category: "economy",
    description: "Creates an RPG account",
    usage: "register [knight OR archer OR mage]"
  };