exports.run = async (client, message, args, level) => { // eslint-disable-line no-unused-vars

  };
  
  exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: ["i", "inv"],
    permLevel: "User"
  };
  
  exports.help = {
    name: "inventory",
    category: "economy",
    description: "Shows items you own.",
    usage: "inventory"
  };