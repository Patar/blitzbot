const { Canvas } = require("canvas-constructor");
const { resolve, join } = require("path"); 
const { Attachment } = require("discord.js");
const { get } = require("snekfetch");
const fetch = require('node-fetch');
const imageUrlRegex = /\?size=2048$/g;

async function profile(member, score){
  const { level, exp } = score;
  const { body: avatar } = await get(member.user.displayAvatarURL.replace(imageUrlRegex, "?size=128"));

  let curLevel = Math.floor(0.1 * Math.sqrt(exp));
  let nextLevel = curLevel + 1;

  let nextLevelExp = Math.floor(100 * (nextLevel * nextLevel));
  let thisLevelExp = Math.floor(100 * (curLevel * curLevel));

  let botExp = thisLevelExp;
  let topExp = nextLevelExp - thisLevelExp;

  let mult = (exp - botExp) / topExp;
  let bar = (mult.toFixed(2));
  let name = member.displayName.length > 20 ? member.displayName.substring(0, 17) + "..." : member.displayName;

  return new Canvas(550, 550)
  .setColor("#7289DA")
  .addRect(0, 0, 550, 350)
  .setColor("#000000")
  .addRect(98, 273, 354, 34)
  .setColor("#2C2F33")
  .addRect(100, 275, 350, 30)
  .addRect(0, 350, 550, 200)
  .setColor("#00A86B")
  .addRect(100, 275, 350 * bar, 30)
  .setColor("#FFFFFF")
  .setShadowColor("rgba(22, 22, 22, 1)")
  .setShadowOffsetY(5)
  .setShadowBlur(10)
  .save()
  .setColor("#2C2F33")
  .addCircle(275, 190, 20)
  .addCircle(275, 125, 64)
  .restore()
  .addRoundImage(avatar, 211, 61, 128, 128, 64)
  .restore()
  .setColor("#FFFFFF")
  .setTextAlign('center')
  .setTextFont('20pt Impact')
  .addText(name, 275, 70)
  .setTextFont('18pt Impact')
  .addText(`${level}`, 275, 200)
  .setTextFont('17pt Impact')
  .addText(`${exp} / ${nextLevelExp}`, 275, 299)
  .toBuffer();
}

// eslint-disable-next-line no-unused-vars
exports.run = async (client, message, args, level) => {
  
  if (message.guild) {
    
    const key = `${message.author.id}-eco`;    
    
    if(client.economy.has(key)) {
    await message.channel.send(new Attachment(await profile(message.member, client.economy.get(key)), `profilecard.png`));
  } else {
    message.channel.send('Please create a profile using \`b!register\`');
  }
}
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "User"
};

exports.help = {
  name: "profile",
  category: "economy",
  description: "Display user profile.",
  usage: "profile"
};