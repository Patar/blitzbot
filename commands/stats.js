const { version } = require("discord.js");
const moment = require("moment");
const chalk = require('chalk');
require("moment-duration-format");

exports.run = (client, message, args, level) => { // eslint-disable-line no-unused-vars
  message.reply('Check the console.');
  const duration = moment.duration(client.uptime).format(" D [days], H [hrs], m [mins], s [secs]");
  client.logger.log(`[LOG]
${chalk.black('__________________________________')}${chalk.cyan('• Mem Usage')}  :: ${(process.memoryUsage().heapUsed / 1024 / 1024).toFixed(2)} MB
${chalk.black('__________________________________')}${chalk.cyan('• Uptime')}     :: ${duration}
${chalk.black('__________________________________')}${chalk.cyan('• Users')}      :: ${client.users.size.toLocaleString()}
${chalk.black('__________________________________')}${chalk.cyan('• Servers')}    :: ${client.guilds.size.toLocaleString()}
${chalk.black('__________________________________')}${chalk.cyan('• Channels')}   :: ${client.channels.size.toLocaleString()}
${chalk.black('__________________________________')}${chalk.cyan('• Discord.js')} :: v${version}
${chalk.black('__________________________________')}${chalk.cyan('• Node')}       :: ${process.version}`);
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "Bot Admin"
};

exports.help = {
  name: "stats",
  category: "Miscelaneous",
  description: "Gives some useful bot statistics",
  usage: "stats"
};
