const fs = require('fs');
require("moment-duration-format");

exports.run = async (client, message, args, level) => { // eslint-disable-line no-unused-vars

    if(!typeof args[0]) return;
    let bUser = message.mentions.users.first().id || args[0];

    fs.readFile('./util/banned.json', (err, data) => {
        if(err) return;
        let obj = JSON.parse(data);
        if(obj.users.includes(bUser)) return;
        obj.users.push(bUser);
        let json = JSON.stringify(obj);
        fs.writeFile('./util/banned.json', json, err => { if(err) return; });
        client.logger.log(`[LOG] User ${bUser} was banned.`);
    });
};

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: ["ban"],
  permLevel: "Bot Admin"
};

exports.help = {
  name: "blacklist",
  category: "System",
  description: "Makes user unable to use the bot.",
  usage: "blacklist [user]"
};
